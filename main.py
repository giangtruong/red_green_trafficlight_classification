
import utilities as ult

def main():
    
    filename = input("Image name: ")
    print("0 is Red trafficlight / 1 is Green trafficlight")
    print(ult.predict_image(filename))
    
if __name__ == '__main__' :
    main()