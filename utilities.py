import pickle
import cv2

def init():
    model = pickle.load(open("final_model.sav", "rb"))
    return model

def predict_image(filename):
    model = init()

    img = cv2.imread(filename, 1)
    img = cv2.resize(img, (100, 100))
    img = img.reshape(-1)

    test = [img]
    return model.predict(test)